import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Owner } from '../../../core/models/owner.model';
import { UserService } from '../../../core/services/user.service';

@Component({
	selector: 'app-user-polls',
	templateUrl: './user-polls.component.html',
	styleUrls: ['./user-polls.component.scss'],
})
export class UserPollsComponent implements OnInit {
	public _user: Observable<Owner> = this.userService.user;
	public isModalOpened = false;
	public pollsAreLoaded = false;

	constructor(private userService: UserService) {}

	ngOnInit(): void {}

	sendRetrieveEmail() {
		alert('TODO');
	}
}
