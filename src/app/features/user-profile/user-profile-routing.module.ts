import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserPollsComponent } from './user-polls/user-polls.component';

const routes: Routes = [{ path: 'polls', component: UserPollsComponent }];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class UserProfileRoutingModule {}
