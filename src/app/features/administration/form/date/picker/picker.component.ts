import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
	selector: 'app-picker',
	templateUrl: './picker.component.html',
	styleUrls: ['./picker.component.scss'],
})
export class PickerComponent implements OnInit {
	@Input()
	public form: UntypedFormGroup;
	selectionKind = 'multiple';
	dateCalendarEnum: any[];
	today: Date = new Date();

	constructor() {}

	ngOnInit(): void {}

	setDefaultDatesForInterval() {}
}
