import { Component, Input } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { Poll } from '../../../core/models/poll.model';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { DateUtilitiesService } from '../../../core/services/date.utilities.service';
import { ClipboardService } from 'ngx-clipboard';
import { ToastService } from '../../../core/services/toast.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-success',
	templateUrl: './success.component.html',
	styleUrls: ['./success.component.scss'],
})
export class SuccessComponent {
	@Input() poll: Poll;
	mailToRecieve: string;
	window: any = window;
	environment = environment;
	today: Date = new Date();

	constructor(
		public pollService: PollService,
		private dateUtils: DateUtilitiesService,

		private toastService: ToastService,
		private titleService: Title
	) {
		this.titleService.setTitle(
			environment.appTitle + ' - 🎉 succès de création de sondage - ' + this.pollService.form.value.title
		);
	}

	sendToEmail() {
		alert('todo');
	}
}
